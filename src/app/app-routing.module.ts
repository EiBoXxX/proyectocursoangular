import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { ListaComponent } from './lista/lista.component';
import { DetalleComponent } from './detalle/detalle.component';
import { GrabarComponent } from './grabar/grabar.component';
import { LoginGuardGuard } from "./login-guard.guard";


const routes: Routes = [
  {path: '', component: LoginComponent},
  {path: 'listado', component: ListaComponent, canActivate: [LoginGuardGuard]},
  {path: 'listado/detalle', component: DetalleComponent},
  {path: 'grabar', component: GrabarComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
